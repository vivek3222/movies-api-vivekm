/* eslint-disable max-len */
/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
const { dbConnection, dbDetails } = require('../src/utils/connection');

const con = dbConnection(dbDetails);

// ? -------------------------------- Show Data From Director Table ---------------------------------------------

const getAllDirectorData = tableName => new Promise((resolve, reject) => {
  const sql = `SELECT * FROM ${tableName} `;
  con.query(sql, (error, result) => {
    if (error) throw error;
    resolve(result);
  });
});
// getAllTableData('directorsData').then(data => console.log(data));

// ?--------------------------------- Show Data From Movies Table -----------------------------------------------

const getAllMoviesData = tableName => new Promise((resolve, reject) => {
  const sql = `SELECT moviesData.*, directorsData.Director_Name From ${tableName} 
               LEFT JOIN directorsData on moviesData.Director_id = directorsData.Id`;
  con.query(sql, (error, result) => {
    if (error) throw error;
    resolve(result);
  });
});
// getAllMoviesData('moviesData').then(data => console.log(data));

// ? --------------------------------- GET TABLE DATA WITH ID ---------------------------------------------------

const getDataWithId = (tableName, id) => new Promise((resolve, reject) => {
  const sql = `SELECT * FROM ${tableName} WHERE Id = ${id}`;
  con.query(sql, (error, result) => {
    if (error) throw error;
    resolve(result);
  });
});
// getDataWithId('directorsData', '5').then(data => console.log(data));
// getDataWithId('directorsData', '5').then(data => console.log(data));

// ? --------------------------------- ADD NEW DIRECTOR ------------------------------------------------------------

const addNewDirector = (tableName, name) => new Promise((resolve, reject) => {
  const sql = `INSERT INTO ${tableName} (director_Name) VALUES ("${name}")`;
  con.query(sql, (error, result) => {
    if (error) throw error;
    resolve(result);
  });
});
// addNewDirector('directorsData', 'Anthony Russo').then(data => console.log(data));
// addNewDirector('directorsData', 'Joe Russo').then(data => console.log(data));

// ? -------------------------------- UPDATE DIRECTOR WITH ID ---------------------------------------------------

const updateDirectorWithId = (tableName, id, directorName) => new Promise((resolve, reject) => {
  const sql = `UPDATE ${tableName} SET director_Name = '${directorName}' WHERE Id = ${id}`;
  con.query(sql, (error, result) => {
    if (error) throw error;
    resolve(result);
  });
});
// updateDirectorWithId('directorsData', '37', 'Joe Russo').then(data => console.log(data));

// ? --------------------------------- DELETE DATA WITH ID -------------------------------------------------------

const deleteDataWithId = (tableName, id) => new Promise((resolve, reject) => {
  const sql = `DELETE FROM ${tableName} WHERE Id = ${id}`;
  con.query(sql, (error, result) => {
    if (error) throw error;
    resolve(result);
  });
});
// deleteDataWithId('directorsData', '37').then(data => console.log(data));
// deleteDataWithId('moviesData', '50').then(data => console.log(data));

// ? ----------------------------- INSERT MOVIES DATA -------------------------------------------------------------
const insertData = {
  Rank: '51',
  Title: 'Dawn Of Justice',
  director_Id: 38,
  Rating: 8.7,
  Metascore: 91,
};
const insertMovieData = (tableName, Data) => new Promise((resolve, reject) => {
  con.query(`INSERT INTO ${tableName} SET ?`, Data, (error, result) => {
    if (error) throw error;
    console.log(result);
    resolve(result);
  });
});
// insertMovieData('moviesData', insertData);

// ?--------------------------------- UPDATE MOVIES DATA WITH GIVEN ID-----------------------------------------------------------
const updateData = {
  Description: 'Awesome movie',
  Runtime: 175,
  Genre: 'Action',
};
const updateMovieData = (tableName, Data, id) => new Promise((resolve, reject) => {
  con.query(`UPDATE ${tableName} SET ? WHERE Id = ${id}`, Data, (error, result) => {
    if (error) throw error;
    console.log(result);
    resolve(result);
  });
});
// updateMovieData('moviesData', updateData, 51);
// con.end();
