const Joi = require('@hapi/joi');

const moviesSchema = {
  ID: Joi.number().min(1),
  Title: Joi.string().min(3).max(50).required(),
  Rank: Joi.number().min(1),
  Description: Joi.string().min(10),
  Runtime: Joi.number().min(10),
  Genre: Joi.string().min(1),
  Rating: Joi.number().min(1),
  Metascore: Joi.string(),
  Votes: Joi.number(),
  Gross_Earning_in_Mil: Joi.string(),
  Director_id: Joi.number().min(1).required(),
  Actor: Joi.string().min(3),
  Year: Joi.number().min(1800).max(2022),
};

const directorsSchema = {
  Id: Joi.number().min(1),
  Director_Name: Joi.string().min(3).max(30).required(),
};

module.exports = {
  moviesSchema,
  directorsSchema,
};
