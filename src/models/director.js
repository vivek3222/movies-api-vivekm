/* eslint-disable no-unused-vars */
const { dbConnection, dbDetails } = require('../utils/connection');
const { directorsSchema } = require('../models/validation');

const con = dbConnection(dbDetails);

const getAllDirectorData = tableName => new Promise((resolve, reject) => {
  const sql = `SELECT * FROM ${tableName} `;
  con.query(sql, (error, result) => {
    if (error) {
      reject(error);
    }
    resolve(result);
  });
});

const getDirectorWithId = id => new Promise((resolve, reject) => {
  const sql = `SELECT * FROM directorsData WHERE Id = ${id}`;
  con.query(sql, (error, result) => {
    if (error) {
      reject(error);
    }
    resolve(result);
  });
});

const addNewDirector = data => new Promise((resolve, reject) => {
  con.query('INSERT INTO directorsData SET ?', data, (error, result) => {
    if (error) {
      reject(error);
    }
    resolve(result);
  });
});

const updateDirectorWithId = (data, id) => new Promise((resolve, reject) => {
  con.query(`UPDATE directorssData SET ? WHERE Id = ${id}`, data, (error, result) => {
    if (error) {
      reject(error);
    }
    resolve(result);
  });
});

const deleteDirectorWithId = id => new Promise((resolve, reject) => {
  const sql = `DELETE FROM directorsData WHERE Id = ${id}`;
  con.query(sql, (error, result) => {
    if (error) {
      reject(error);
    }
    resolve(result);
  });
});

module.exports = {
  getAllDirectorData,
  getDirectorWithId,
  addNewDirector,
  updateDirectorWithId,
  deleteDirectorWithId,
  directorsSchema,
};
