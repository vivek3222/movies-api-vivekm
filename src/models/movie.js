/* eslint-disable no-shadow */
/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
const { dbConnection, dbDetails } = require('../utils/connection');
const { moviesSchema } = require('../models/validation');

const con = dbConnection(dbDetails);

const getAllMoviesData = tableName => new Promise((resolve, reject) => {
  const sql = `SELECT *, directorsData.Director_Name From ${tableName} 
                 LEFT JOIN directorsData on moviesData.Director_id = directorsData.Id`;
  con.query(sql, (error, result) => {
    if (error) {
      reject(error);
    }
    resolve(result);
  });
});

const getMovieWithId = id => new Promise((resolve, reject) => {
  const sql = `SELECT *, directorsData.Director_Name FROM moviesData 
               LEFT JOIN directorsData on moviesData.Director_id = directorsData.Id  
               WHERE moviesData.Id = ${id}`;
  con.query(sql, (error, result) => {
    if (error) {
      reject(error);
    }
    resolve(result);
  });
});

const updateMovieData = (data, id) => new Promise((resolve, reject) => {
  con.query(`UPDATE moviesData SET ? WHERE ID = ${id}`, data, (error, result) => {
    if (error) {
      reject(error);
    }
    resolve(result);
  });
});

const insertMovieData = data => new Promise((resolve, reject) => {
  con.query('INSERT INTO moviesData SET ?', data, (error, result) => {
    if (error) {
      reject(error);
    }
    resolve(result);
  });
});

const deleteMovieWithId = id => new Promise((resolve, reject) => {
  const sql = `DELETE FROM moviesData WHERE Id = ${id}`;
  con.query(sql, (error, result) => {
    if (error) {
      reject(error);
    }
    resolve(result);
  });
});

module.exports = {
  getAllMoviesData, getMovieWithId, insertMovieData, updateMovieData, deleteMovieWithId, moviesSchema,
};
