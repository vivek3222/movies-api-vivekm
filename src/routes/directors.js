/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
const express = require('express');
const Joi = require('@hapi/joi');
const director = require('../models/director');

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const response = await director.getAllDirectorData('directorsData');
    res.send(response);
  } catch (error) {
    console.log(error);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const result = Joi.validate(req.body, director.directorsSchema);
    const response = await director.getDirectorWithId(req.params.id);
    res.send(response);
  } catch (error) {
    console.log(error);
  }
});

router.post('/', async (req, res) => {
  try {
    const result = Joi.validate(req.body, director.directorsSchema);
    const response = await director.addNewDirector(req.body);
    res.send(response);
  } catch (error) {
    console.log(error);
  }
});

router.put('/:id', async (req, res) => {
  try {
    const result = Joi.validate(req.body, director.directorsSchema);
    const response = director.updateDirectorWithId(req.body, req.params.Id);
    res.send(response);
  } catch (error) {
    console.log(error);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const result = Joi.validate(req.body, director.directorsSchema);
    const response = await director.deleteDirectorWithId(req.params.id);
    res.send(response);
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
