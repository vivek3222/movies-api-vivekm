/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
const express = require('express');
const Joi = require('@hapi/joi');
const movie = require('../models/movie');

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const response = await movie.getAllMoviesData('moviesData');
    res.send(response);
  } catch (error) {
    console.log(error);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const result = Joi.validate({ ID: req.params.id }, movie.moviesSchema);
    if (result.error === null) {
      const response = await movie.getMovieWithId(req.params.id);
      if (response.length === 0) {
        res.status(400).send('BAD REQUEST');
      } else {
        res.status(200).send(response);
      }
    } else {
      res.status(404).send(result.error.details[0].message);
    }
  } catch (error) {
    next(error);
  }
});


router.post('/', async (req, res, next) => {
  try {
    const result = Joi.validate(req.body, movie.moviesSchema);
    if (result.error === null) {
      const response = await movie.insertMovieData(req.body);
      if (response.length === 0) {
        res.status(400).send('BAD REQUEST');
      } else {
        res.status(201).send(response);
      }
    } else {
      res.status(400).send(result.error.details[0].message);
    }
  } catch (error) {
    next(error);
  }
});

router.put('/:id', async (req, res) => {
  try {
    const result = Joi.validate(req.body, movie.moviesSchema);
    if (result.error === null) {
      const response = await movie.updateMovieData(req.body, req.params.id);
      if (result.length === 0) {
        res.status(500).send('INTERNAL SERVER ERROR');
      } else {
        res.status(202).send(response);
      }
    } else {
      res.status(400).send(result.error.details[0].message);
    }
  } catch (error) {
    console.log(error);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const result = Joi.validate(req.body, movie.moviesSchema);
    if (result.error === null) {
      const response = await movie.deleteMovieWithId(req.params.id);
      if (result.length === 0) {
        res.status(500).send('INTERNAL SERVER ERROR');
      } else {
        res.status(200).send(response);
      }
    } else {
      res.status(400).send(result.error.details[0].message);
    }
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
