/* eslint-disable no-shadow */
const mysql = require('mysql');

const dbDetails = {
  host: '172.17.0.2',
  user: 'root',
  password: 'root',
  database: 'movies',
};

function dbConnection(dbDetails) {
  return mysql.createConnection(dbDetails);
}
module.exports = { dbDetails, dbConnection };
