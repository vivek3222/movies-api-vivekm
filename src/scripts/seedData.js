
/* eslint-disable no-shadow */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
/* eslint-disable max-len */
/* eslint-disable no-console */
const { dbConnection, dbDetails } = require('../utils/connection');
const movies = require('../jsonData/movies.json');

const con = dbConnection(dbDetails);

// ? -------------------------------------- FILTER DATA FOR NA ---------------------------------------------------
const dataFilter = (movies) => {
  movies.forEach((obj) => {
    Object.keys(obj).forEach((element) => {
      if (obj[element] === 'NA') {
        obj[element] = null;
      }
    });
  });
  return movies;
};

// ? ------------------------------------- SET UNIQUE DIRECTOR  ---------------------------------------------------

const uniqueDirectors = (movies) => {
  const director = [];
  const uniqueDirectors = new Set();
  movies.forEach((movie) => {
    uniqueDirectors.add(movie.Director);
  });
  uniqueDirectors.forEach((element) => {
    director.push(element);
  });
  return director;
};

// ?-------------------------------------- DROP TABLE IF EXISTS ---------------------------------------------------

const dropTable = name => new Promise((resolve, reject) => {
  con.query(`DROP TABLE IF EXISTS ${name}`, (error, result) => {
    if (error) {
      reject(error);
    }
    console.log(`Table Dropped ${name}`);
    resolve(result);
  });
});

// ?-------------------------------------- CREATE TABLE ----------------------------------------------------------

const createTable = (name, query) => new Promise((resolve, reject) => {
  const sql = `CREATE TABLE ${name} ${query} `;
  con.query(sql, (error, result) => {
    if (error) {
      reject(error);
    }
    console.log(`Table Created ${name}`);
    resolve(result);
  });
});

// ? ----------------------------- INSERT DATA INTO DIRECTORS TABLE -----------------------------------------------


const insertDirectorData = (movies) => {
  const directors = uniqueDirectors(movies);
  const promises = directors.map(element => new Promise((resolve, reject) => {
    con.query(`INSERT INTO directorsData(Director_Name) VALUES ('${element}')`, (error, result) => {
      if (error) {
        reject(error);
      }
      resolve(result);
    });
  }));
  return Promise.all(promises);
};
// insertDirectorData(movies);

// ? ----------------------------------INSERTING DATA INTO MOVIES TABLE ---------------------------------------------

const insertMoviesDetails = (movies) => {
  const movie = dataFilter(movies);
  const promises = movie.map(element => new Promise((resolve, reject) => {
    const sql = `INSERT INTO moviesData (Rank, Title, Description, Runtime, Genre, Rating, Metascore, Votes, Gross_Earning_in_Mil, Director_id, Actor, Year )
       VALUES (${element.Rank}, "${element.Title}", "${element.Description}", ${element.Runtime}, "${element.Genre}", ${element.Rating}, ${element.Metascore}, ${element.Votes},
        ${element.Gross_Earning_in_Mil},(select Id from directorsData where Director_Name = "${element.Director}"), "${element.Actor}", ${element.Year})`;
    con.query(sql, (error, result) => {
      if (error) {
        reject(error);
      }
      resolve(result);
    });
  }));
  return Promise.all(promises);
};

// insertMoviesDetails(movies);

const callFunction = async () => {
  try {
    await dropTable('moviesData');
    await dropTable('directorsData');
    await createTable('directorsData', '(Id INT AUTO_INCREMENT PRIMARY KEY, Director_Name VARCHAR(50) NOT NULL)');
    await createTable('moviesData', `(ID INT PRIMARY KEY AUTO_INCREMENT, 
    Rank INT, Title VARCHAR(50) NOT NULL, Description LONGTEXT, 
    Runtime INT(11), Genre VARCHAR(50), Rating FLOAT, Metascore INT, 
    Votes BIGINT, Gross_Earning_in_Mil INT, Director_id INT NOT NULL, 
    FOREIGN KEY (Director_id) REFERENCES directorsData(Id) ON UPDATE 
    CASCADE ON DELETE CASCADE, Actor VARCHAR(50), Year INT(11))`);
    await insertDirectorData(movies).then(() => console.log('Directors Data Inserted'));
    await insertMoviesDetails(movies).then(() => { console.log('Movies Data Inserted'); });
  } catch (error) {
    console.log(error);
  }
};
callFunction();
// con.end();
