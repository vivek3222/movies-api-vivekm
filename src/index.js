/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable import/no-extraneous-dependencies */
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');

const app = express();
app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', (req, res) => {
  res.send('Hello World');
});

app.use('/api/movies', require('./routes/movies'));
app.use('/api/directors', require('./routes/directors'));

app.use(morgan('dev', {
  skip(req, res) { return res.statusCode < 400; },
}));
const accessLogStream = fs.createWriteStream(path.join(__dirname, './utils/access.log'), { flags: 'a' });
app.use(morgan('combined', { stream: accessLogStream }));

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`App listening on port ${port}!`));
